const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const hostname = "0.0.0.0";
const port = 3000;

const server = express();
mongoose.connect("mongodb://localhost:27017/viva-back");
if(mongoose.connection.readyState == 1) {
  console.log("Connected to database");
}
server.use(cors());
server.use(express.urlencoded());
server.use(express.json());

const userRoutes = require("./api/routes/userRoutes");
userRoutes(server);


server.listen(port, hostname);

